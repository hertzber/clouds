from typing import List, Dict, Any


def main_mapper(map_dict: str) -> list[dict[str, int]]:
    res_list = []

    for line_number in map_dict:
        line_string = map_dict[line_number]
        # Split string at whitespace
        line_string_array = line_string.split(' ')
        for word in line_string_array:
            a = {word: 1}
            res_list.append(a)

    return res_list


def textFileFormat(text: list[str]):
    res_dict = {}
    for i in range(len(text)):
        res_dict[i] = text[i]

    return res_dict


def shuffler(map_dict_list: str) -> list[str]:
    res_list = []

    for kv_pair in map_dict_list:
        key = list(kv_pair.keys())[0]
        counter = map_dict_list.count(kv_pair)
        value_to_add = {key: [1] * counter}
        if value_to_add not in res_list:
            res_list.append(value_to_add)

    return list(res_list)


def reducer(map_dict: str) -> dict[str, int]:
    res_dict = {}
    key = list(map_dict.keys())[0]
    value = list(map_dict.values())[0]
    res_dict[key] = len(value)
    return res_dict


if __name__ == '__main__':
    a = {1 : "Detta är en rad", 2: "Detta är andra raden", 3 : "Detta är tredje raden"}

    print(list(a.keys()))
    print(list(a.values()))
    b = "Detta är en rad"

    c = b.split(" ")

    print(c)
    with open('test.txt') as f:
        text = f.read().splitlines()

    print(text)

    d = textFileFormat(text)
    print(d)

    print("MAIN MAPPER:")

    e = main_mapper(d)
    print(e)

    print("SHUFFLER")

    f = shuffler(e)



    print("REDUCER")

    for i in f:
        a = reducer(i)
        print(a)

# This function is not intended to be invoked directly. Instead it will be
# triggered by an HTTP starter function.
# Before running this sample, please:
# - create a Durable activity function (default name is "Hello")
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import json
import os

import azure.functions as func
import azure.durable_functions as df


def orchestrator_function(context: df.DurableOrchestrationContext):
    #file_dict = read_file_and_format("test.txt")

    yield context.call_activity('GetInputDataFn', "")
    result_array = []
    
    abspath = os.getcwd()

    for file in os.listdir("data/"):
        file_dict = read_file_and_format(F"{abspath}/{file}")
        result1 = yield context.call_activity('Mapper', file_dict)
        result2 = yield context.call_activity('Shuffler', result1)
        result3 = []
        for shuffled in result2:
            a = context.call_activity('Reducer', shuffled)
            result3.append(a)
        result3 = yield context.task_all(result3)

        inner_file_dict = {file: [
            {"Mapper": result1},
            {"Shuffler": result2},
            {"Reducer": result3}
        ]
        }

      #   {"Mapper": result1}, {"Shuffler:" result2}, {"Reducer": result3}
        result_array.append(inner_file_dict)
    return result_array


def read_file_and_format(filepath: str):
    with open(filepath) as f:
        text = f.read().splitlines()
    res_dict = {}
    for i in range(len(text)):
        res_dict[i] = text[i]

    return res_dict


main = df.Orchestrator.create(orchestrator_function)

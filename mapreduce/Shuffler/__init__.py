# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging


def main(mapdictlist: list[dict]) -> list[dict[str, list[int]]]:
    res_list = []

    for kv_pair in mapdictlist:
        key = list(kv_pair.keys())[0]
        counter = mapdictlist.count(kv_pair)
        value_to_add = {key: [1] * counter}
        if value_to_add not in res_list:
            res_list.append(value_to_add)
    return list(res_list)

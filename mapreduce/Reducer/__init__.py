# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging


def main(mapdict: dict) -> dict[str, int]:
    res_dict = {}
    key = list(mapdict.keys())[0]
    value = list(mapdict.values())[0]
    res_dict[key] = len(value)
    return res_dict

# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import os
from azure.identity import DefaultAzureCredential
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient
from io import BytesIO
from tempfile import NamedTemporaryFile

def main(name: str):

    try:
        print("Azure Blob Storage Python quickstart sample")
        connect_str = os.environ.get('AZURE_STORAGE_CONNECTION_STRING')

        # Create the BlobServiceClient object
        blob_service_client = BlobServiceClient.from_connection_string(
            connect_str)
        print("BLOB SERVICE CLIENT PRINT: ", blob_service_client)

        # Get the Container Client§
        container_client = blob_service_client.get_container_client(
            container="mrinputs")
        print("Container Client: ", container_client)

        print("ALL BLOBS:")
        blob_list = container_client.list_blobs()
        abspath = os.getcwd()
        blob_arr = []
        local_path = F"{abspath}/"
    

        for blob in blob_list:
            
            
            print("\t" + blob.name)
           
            blob_arr.append(blob)
           
           
            download_file_path = os.path.join(local_path, str.replace(
                blob.name, '.txt', 'D.txt'))
            print("\nDownloading blob to \n\t" + download_file_path)
            with open(file=download_file_path, mode="wb") as download_file:
                download_file.write(
                    container_client.download_blob(blob.name).readall())
       
        
        # print("ABSPATH:" , str(abspath))
        
        
        #list_of_files = os.listdir(F"{abspath}/data")
        # print(blob_arr)
        return "blob_arr"

    except Exception as ex:
        print('Exception:')
        print(ex)

    return "HEJJ"

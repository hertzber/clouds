# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging


def main(mapdict: dict) -> list[dict]:
    res_list = []
    for line_number in mapdict:
        line_string = mapdict[line_number]
        # Split string at whitespace
        line_string_array = line_string.split(' ')
        for word in line_string_array:
            a = {word: 1}
            res_list.append(a)

    return res_list

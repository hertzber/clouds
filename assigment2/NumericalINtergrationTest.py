import math
import unittest
from Microservice import NumericalIntegration


class MyTestCase(unittest.TestCase):
    def test_abs_sin_of_x(self):
        a = NumericalIntegration.abs_sin_off_X(math.pi)
        b = NumericalIntegration.abs_sin_off_X(0)
        c = NumericalIntegration.abs_sin_off_X(math.pi / 2)
        self.assertAlmostEqual(a, 0, delta=0.1)
        self.assertAlmostEqual(b, 0, delta=0.1)
        self.assertAlmostEqual(c, 1, delta=0.1)

    def test_calculate(self):
        ni = NumericalIntegration(100, 0, math.pi)
        res = ni.calculate()
        self.assertAlmostEqual(res, 2, delta=0.1, msg=f'The result is: {res}')

        ni2 = NumericalIntegration(1000, 0, math.pi / 2)
        res2 = ni2.calculate()
        self.assertAlmostEqual(res2, 1, delta=0.1)

        ni3 = NumericalIntegration(100, 0, math.pi / 3)
        res3 = ni3.calculate()
        self.assertAlmostEqual(res3, 0.5, delta=0.1)

    def test_calculate_n_one(self):
        ni = NumericalIntegration(1, 0, math.pi)
        res = ni.calculate()
        self.assertAlmostEqual(res, math.pi, delta=0.1, msg=f'The result is: {res}')



if __name__ == '__main__':
    unittest.main()

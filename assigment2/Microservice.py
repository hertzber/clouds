from flask import Flask
import math
import json

app = Flask(__name__)


@app.route("/")
def mainpage():
    print(" TEst ")
    return """
    <p> Hello Flask </p>
    """


def numerical_integral_loop(lo, up):
    intervals = [1, 10, 100, 1000, 10 ** 4, 10 ** 5, 10 ** 6]
    res_dict = {}
    for sub_intervals in intervals:
        ni = NumericalIntegration(sub_intervals, lo, up)
        res = ni.calculate()
        res_dict[sub_intervals] = res

    print(res_dict)

    return res_dict


@app.route("/numericalintegralservice/<lo>/<up>")
def get_boundaries(lo: int, up: int):
    print(lo, up)
    lo = float(lo)
    up = float(up)

    res = numerical_integral_loop(lo, up)

    res = json.dumps(res)

    return F"""
    <p>lo:{float(lo)} hi:{float(up)}</p>
    
    <p> Result of numerical integration: <b>{res}</b> </p>
    """


class NumericalIntegration:
    def __init__(self, number_of_sub_intervals, lower, upper):
        if number_of_sub_intervals < 0:
            raise "Number must be greater than 0"
        self.N = number_of_sub_intervals
        self.lo = lower
        self.up = upper

    @staticmethod
    def abs_sin_off_X(x):
        return abs(math.sin(x))

    def calculate(self):
        integral = 0
        width = (self.up - self.lo) / self.N
        delta = width * 0.5

        for i in range(self.N):
            height = self.abs_sin_off_X(i + delta)

            integral += width * height
        return integral


if __name__ == '__main__':

    numerical_integral_loop(0, 3.14)
    # app.run()
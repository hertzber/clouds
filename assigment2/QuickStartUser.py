from locust import HttpUser, task, run_single_user


class QuickStartUser(HttpUser):
    #host = "https://numintergral.azurewebsites.net"
    # host = "http://20.107.18.124"
    # host = "https://integral.azurewebsites.net"
    host = "http://127.0.0.1:5000"

    @task
    def hello_world(self):
        self.client.get("/numericalintegralservice/0/3.14")


if __name__ == '__main__':
    run_single_user(QuickStartUser)

# https://numintergral.azurewebsites.net/api/httptrigger1?lo=0&up=3.14

# 20.107.18.124


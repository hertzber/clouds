import logging

import math

import azure.functions as func


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')
    a = req.params.items
    logging.info(F'params is {a}')
    lo = req.params.get('lo')
    up = req.params.get('up')

    if not lo:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            lo = req_body.get('lo')

    if not up:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            up = req_body.get('up')

    if lo and up:
        result = numerical_integral_loop(lo=lo, up=up)
        return func.HttpResponse(f"The result of the integration is: \n{result}")
    else:
        return func.HttpResponse(
            "Quarey in the URL with variables 'lo' and 'up' to calculate a numerical integral. Example:http://localhost:7071/api/HttpTrigger1?lo=0&up=3.14",
            status_code=200
        )


class NumericalIntegration:
    def __init__(self, number_of_sub_intervals, lower, upper):
        if number_of_sub_intervals < 0:
            raise "Number must be greater than 0"
        self.N = number_of_sub_intervals
        self.lo = lower
        self.up = upper

    @staticmethod
    def abs_sin_off_X(x):
        return abs(math.sin(x))

    def calculate(self):
        integral = 0
        width = (self.up - self.lo) / self.N
        delta = width * 0.5

        for i in range(self.N):
            height = self.abs_sin_off_X(i + delta)

            integral += width * height
        return integral


def numerical_integral_loop(lo, up):
    lo = float(lo)
    up = float(up)
    intervals = [1, 10, 100, 1000, 10 ** 4, 10 ** 5, 10 ** 6]
    res_dict = {}
    for sub_intervals in intervals:
        ni = NumericalIntegration(sub_intervals, lo, up)
        res = ni.calculate()
        res_dict[sub_intervals] = res

    print(res_dict)

    return res_dict


# app = Flask(__name__)


# @app.route("/")
# def mainpage():
#     print(" TEst ")
#     return """
#     <p> Hello Flask </p>
#     """


# @app.route("/numericalintegralservice/<lo>/<up>")
# def get_boundaries(lo: int, up: int):
#     print(lo, up)
#     lo = float(lo)
#     up = float(up)

#     res = numerical_integral_loop(lo, up)

#     res = json.dumps(res)

#     return F"""
#     <p>lo:{float(lo)} hi:{float(up)}</p>

#     <p> Result of numerical integration: <b>{res}</b> </p>
#     """
